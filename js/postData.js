(function(){
    window.postData = (url = '', data = {}, bodytype = '', customHeaders = {}) => {
        if(bodytype === 'formdata')
        {
            var formData = new FormData();
            for(var key in data)
            {
                formData.append(key, data[key]);
            }
            data = formData;
        }
        else
        {
            customHeaders["Content-Type"] = "application/json; charset=utf-8";
            data = JSON.stringify(data);
        }

        return fetch(url, {
            method: "post", 
            mode: "cors", 
            cache: "no-cache", 
            headers: customHeaders,
            redirect: "follow", 
            referrer: "no-referrer", 
            body: data
        })
        .then(
            response => response
        ); 
    }
})(window);

